#!/bin/bash

set -e

# dirs
DIR="$( cd "$( dirname "$0" )" && pwd )"

package=$(< $DIR"/package.json")

# vars
VER=$(echo "$package" | jq -r .version)
NAME=$(echo "$package" | jq -r .name)

test() {
    cd $DIR
    node ./tests/run.js
}
lint() {
    cd $DIR
    eslint src --ext .js
}

doc() {
    cd $DIR
    rm -fr jsdoc/*
    jsdoc -r -c ./jsdoc.json -d ./jsdoc/
}

test() {
    cd $DIR
    node ./tests/run.js
}

build() {
    cd $DIR
    echo "build"
}

setup() {
    echo "Setup"
    deps
    symlinks
}

deps() {
    echo -e "Cloning dependencies id deps directory..."
    cd $DIR
    # clear
    rm -fr deps/*/
    # read deps
    local urls=$(echo "$package" | jq -r .deps[])
    # clone
    cd $DIR"/deps"
    for url in ${urls[@]}; do
        git clone $url
    done
    echo -e "Cloning done."
}

symlinks() {
    echo -e "Creating symlinks..."
    cd $DIR
    # read symlinks
    local linkdirs=($(echo "$package" | jq -r '.symlinks | keys[]'))
    for linkdir in ${linkdirs[@]}; do
        echo -e "  > Creating symlinks in "$linkdir
        cd $DIR
        local targets=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | keys_unsorted[]'))
        local links=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | .[]'))
        # create symlinks
        cd $DIR"/"$linkdir
        for (( i=0; i<${#targets[@]}; i++ )); do
            link=${links[$i]}
            target=${targets[$i]}
            if [[ -f $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            elif [[ -d $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            else
                ln -sf $target $link
                echo -e "    > "$target" -> "$linkdir$link" created."
            fi
        done
    done
}

help() {
    usage="project.sh <comman>
    
    Commands:
        lint ...... (dev) run eslint
        test ...... (dev) run tests (needs nodejs)
        jsdoc ..... (dev) generates jsdoc (needs jsdoc and jsdoc-typescript plugin)
        build ..... (dev) build project
        setup ..... (dev) Get depencencies (deps) and Create symlinks to dependencies (symlinks)
        deps ...... (dev) Get depencencies
        symlinks .. (dev) Create symlinks to dependencies
    "
    echo "$usage"
}

# args
case $1 in
    deps)
        deps;;
    symlinks)
        symlinks;;
    setup)
        setup;;
    lint)
        lint;;
    test)
        test;;
    jsdoc)
        doc;;
    build)
        build;;
esac
