title: rnb-logs
date: 2023-09-20
cats: [informatique]
tags: [rnb-js, code, debug]
techs: [javascript, nodejs, deno]
itemtype: WebApplication
projects: [rnb-logs, rnb-js]
source: https://framagit.org/rui.nibau/rnb-logs
issues: https://framagit.org/rui.nibau/rnb-logs/-/issues
pagesassnippets: true
intro: Petite librairie pour disposer d'un outil simple de log.
status: in-process


## Présentation

Cette librairie est une conséquence directe de l'abandon du projet PHP [rnb-php](/projets/rnb-php).

°°todo°°En cours de rédaction...

## Documentation

[°°demo°°JsDoc](/lab/rnb-logs/jsdoc/index.html)

### rnb/logs/Logger

Classe « abstraite » pour définir l'API générique d'un objet chargé de loguer des messages.

[°°api°°iframe:](/lab/rnb-logs/jsdoc/module-rnb_logs_Logger.html)

La classe est utilisée par deux implémentations :

• ``FileLogger`` : pour loguer dans un fichier.
• ``JsLogger`` : pour loguer dans un objet javascript.

## Historique

..include::./changelog.md

## Licence

..include::./licence.md

