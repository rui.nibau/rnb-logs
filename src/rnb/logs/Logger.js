/**@module rnb/logs/Logger */

const LEVELS = {
    trace: 0,
    debug: 1,
    info: 2,
    warn: 3,
    error: 4,
    fatal: 5,
};

/**
 * Log level
 *
 * @typedef {'trace'|'debug'|'info'|'warn'|'error'|'fatal'} LogLevel
 */

/**
 * Options to configure logger
 *
 * @typedef LoggerOptions
 * @property {LogLevel} [level]
 * @property {number} [sizeLimit]
 */

/**
 * Creates a log line : <date> - [<level>] - <message>
 *
 * @param {LogLevel} level 
 * @param {string} message 
 * @returns {string}
 * @protected
 */
export const createLogLine = (level, message) => `\n${(new Date()).toISOString()} - [${level}] - ${message}`;

/**
 * Get a valid Log Level. 'error' when given level is wrong.
 *
 * @param {LogLevel} level
 * @returns {LogLevel} 
 * @protected
 */
export const getLevel = level => level && level in LEVELS ? level : 'error';

/**
 * @param {LogLevel} level 
 * @param {LogLevel} limit 
 * @returns {boolean}
 * @protected
 */
export const canBeLogged = (level, limit) => LEVELS[getLevel(level)] >= LEVELS[getLevel(limit)];

/**
 * Interface to define Logger API
 * 
 * @typedef LoggerInterface
 * @property {LogLevel} level
 * @property {(message: string) => void} trace
 * @property {(message: string) => void} debug
 * @property {(message: string) => void} info
 * @property {(message: string) => void} warn
 * @property {(message: string) => void} error
 * @property {(message: string) => void} fatal
 * @property {() => void} clear
 * @property {() => string[]} read
 * @property {(level: LogLevel, message: string) => void} write
 */

/**
 * Abstract class for Logger
 *
 * @implements {LoggerInterface}
 */
export default class Logger {

    /**
     * @param {LoggerOptions} [options] 
     */
    constructor(options = {}) {
        if (options.level) {
            this.level = options.level;
        }
    }

    /**
     * @type {LogLevel}
     * @private
     */
    __level = 'error';

    /**
     * Log level (error > warn > info). If value is not one of those three levels, 'error' is assumed.
     *
     * @type {LogLevel}
     */
    get level() {
        return this.__level;
    }

    set level(value) {
        this.__level = getLevel(value);
    }

    /**
     * Use Dedicated methods : {@link error}, {@link log}, {@link warn}, etc.
     * 
     * @param {LogLevel} level 
     * @param {string} message 
     * @protected
     */
    write(level, message) {
        if (canBeLogged(level, this.level)) {
            const logline = createLogLine(level, message);
            console.log(logline);
            this._doWrite(logline);
        }
    }

    /**
     * 
     * @param {string} logline 
     * @protected
     */
    _doWrite(logline) {
        throw new Error('Not implemented');
    }

    /**
     * Read logs as a list of lines
     *
     * @returns {string[]}
     */
    read() {
        throw new Error('Not implemented');
    }

    /**
     * Clear logs
     */
    clear() {
        throw new Error('Not implemented');
    }

    /**
     * Log an trace message.
     *
     * @param {string} message 
     */
    trace(message) {
        this.write('trace', message);
    }

    /**
     * Log an debug message.
     *
     * @param {string} message 
     */
    debug(message) {
        this.write('debug', message);
    }

    /**
     * Log an info message.
     *
     * @param {string} message 
     */
    info(message) {
        this.write('info', message);
    }
    
    /**
     * Log a warn message
     *
     * @param {string} message 
     */
    warn(message) {
        this.write('warn', message);
    }

    /**
     * Log a error message
     *
     * @param {string} message 
     */
    error(message) {
        this.write('error', message);
    }

    /**
     * Log an fatal message.
     *
     * @param {string} message 
     */
    fatal(message) {
        this.write('fatal', message);
    }
}
