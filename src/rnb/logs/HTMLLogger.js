/**@module rnb/logs/HTMLLogger */

import { canBeLogged, createLogLine, getLevel } from './Logger.js';

/**
 * @param {string} level
 * @param {string} logline 
 * @returns {HTMLElement}
 * @private
 */
const createLogItem = (level, logline) => {
    const [dateStr, levelStr, ...message] = logline.split(' - ');
    const time = globalThis.document.createElement('time');
    time.textContent = dateStr;
    const strong = globalThis.document.createElement('strong');
    strong.textContent = levelStr;
    const span = globalThis.document.createElement('span');
    span.textContent = message.join(' - ');

    const element = globalThis.document.createElement('div');
    element.dataset.line = logline;
    element.dataset.level = level;
    element.append(time, strong, span);

    return element;
};

/**
 * @typedef {import('./Logger.js').LoggerInterface} LoggerInterface
 * @protected
 */

/**
 * HTML element to display logs
 * 
 * @implements {LoggerInterface}
 */
export default class HTMLLogger extends HTMLElement {

    constructor() {
        super();
        this.level = 'error';
    }

    /**
     * Log level (error > warn > info). If value is not one of those three levels, 'error' is assumed.
     *
     * @type {import('./Logger.js').LogLevel}
     */
    get level() {
        return /**@type {import('./Logger.js').LogLevel}*/(this.dataset.level);
    }

    set level(value) {
        this.dataset.level = getLevel(value);
    }

    /**
     * Use Dedicated methods : {@link error}, {@link log} or {@link warn}
     * 
     * @param {import('./Logger.js').LogLevel} level 
     * @param {string} message 
     * @protected
     */
    write(level, message) {
        if (canBeLogged(level, this.level)) {
            const logline = createLogLine(level, message);
            console.log(logline);
            logline.split(' - ');
            this.append(createLogItem(level, logline));
        }
    }

    /**
     * Read logs as a list of lines
     *
     * @returns {string[]}
     */
    read() {
        const lines = [];
        for (const item of this.children) {
            lines.push(item.dataset.line);
        }
        return lines;
    }

    /**
     * Clear logs
     */
    clear() {
        this.innerHTML = '';
    }

    /**
     * Log an trace message.
     *
     * @param {string} message 
     */
    trace(message) {
        this.write('trace', message);
    }

    /**
     * Log an debug message.
     *
     * @param {string} message 
     */
    debug(message) {
        this.write('debug', message);
    }

    /**
     * Log an info message.
     *
     * @param {string} message 
     */
    info(message) {
        this.write('info', message);
    }
    
    /**
     * Log a wrning message
     *
     * @param {string} message 
     */
    warn(message) {
        this.write('warn', message);
    }
    
    /**
     * Log an error message.
     *
     * @param {string} message 
     */
    error(message) {
        this.write('error', message);
    }

    /**
     * Log an fatal message.
     *
     * @param {string} message 
     */
    fatal(message) {
        this.write('fatal', message);
    }
}

globalThis.customElements.define('rnb-logger', HTMLLogger);
