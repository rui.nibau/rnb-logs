/**
 * @module rnb/logs/FileLogger
 */

import Logger from './Logger.js';

/**
 * @augments Logger
 */
export default class FileLogger extends Logger {

    /**
     * @type {string}
     * @private
     */
    filename = null;

    /**
     * @param {string} path  
     * @param {string} name 
     * @param {import('./Logger.js').LoggerOptions} [options]
     */
    constructor(path, name = 'server', options = {}) {
        super(options);
        this.filename = `${path}/${name}.log`;
    }

    /**
     * Clar logs
     *
     * @ignore
     */
    clear() {
        write(this.filename, '');
    }

    /**
     * @param {string} logline 
     * @protected
     */
    _doWrite(logline) {
        append(this.filename, logline);
    }

    /**
     * Get logs as a list of lines
     * 
     * @returns {string[]}
     * @ignore
     */
    read() {
        return read(this.filename);
    }
}

/**
 * @protected
 * @param {string} filepath 
 * @returns {string[]}
 */
let read = filepath => [];

/**
 * @protected
 * @param {string} filepath 
 * @param {string} logline 
 */
let append = (filepath, logline) => {};

/**
 * @protected
 * @param {string} filepath 
 * @param {string} content 
 */
let write = (filepath, content) => {};

if (typeof Deno === 'object') {
    read = filepath => {
        try {
            const data = Deno.readFileSync(filepath);
            const decoder = new TextDecoder('utf-8');
            return decoder.decode(data).split('\n');
        } catch(error) {
            console.error(error);
            return [];
        }
    };
    write = (filepath, content) => {
        Deno.writeTextFileSync(filepath, content);
    };
    append = (filepath, logline) => {
        Deno.writeTextFileSync(filepath, logline, {append: true});
    };

} else {
    const fs = await import('node:fs');
    read = filepath => {
        try {
            const logs = fs.readFileSync(filepath, {encoding: 'utf-8'});
            return logs.split('\n');    
        } catch(error) {
            console.error(error);
            return [];
        }
    };
    write = (filepath, content) => {
        fs.writeFileSync(filepath, content, {'encoding': 'utf-8'});
    };
    append = (filepath, logline) => {
        fs.appendFile(filepath, logline, () => {});
    };
}
