import Logger from './Logger.js';

/**
 * @module rnb/logs/JsLogger
 */

/**
 * Class to store logs in a javascript array
 */
export default class JsLogger extends Logger {
    /**
     * @type {string[]}
     * @private
     */
    logs = [];

    clear() {
        this.logs.length = 0;
    }

    /**
     * @param {string} logline 
     * @protected
     */
    _doWrite(logline) {
        this.logs.push(logline);
    }

    /**
     * @returns {string[]}
     */
    read() {
        return this.logs;
    }
}
