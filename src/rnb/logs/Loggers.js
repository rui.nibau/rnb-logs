/**@module rnb/logs/Loggers */

import Logger from './Logger.js';

/**
 * @protected
 */
class FakeLogger extends Logger {
    _doWrite(logline) {
        console.log(logline);
    }
    read() {
        return [];
    }
    clear () {}
}

/**
 * 
 * @private
 */
const instances = {};


export default {

    /**
     * @param {string} name 
     * @returns {boolean}
     */
    has(name) {
        return name in instances;
    },

    /**
     * Definie a logger for the given name.
     *
     * @param {string} name 
     * @param {import('./Logger.js').LoggerInterface} logger 
     */
    set(name, logger) {
        instances[name] = logger;
    },

    /**
     * @param {string} name 
     * @returns {import('./Logger.js').LoggerInterface}
     */
    get(name) {
        if (!(name in instances)) {
            instances[name] = new FakeLogger();
        }
        return instances[name];
    },
};

