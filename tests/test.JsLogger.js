import JsLogger from '../src/rnb/logs/JsLogger.js';

console.group('Test: JsLogger');

const logger = new JsLogger();

console.assert(logger.level === 'error', 'JsLogger.level', `actual: "${logger.level}"`, 'expected: "error"');

logger.level = 'info';
console.assert(logger.level === 'info', 'JsLogger.level', `actual: "${logger.level}"`, 'expected: "info"');

logger.level = 'error';

logger.trace('log trace');
console.assert(logger.read().length === 0, 'JsLogger: log < level', `Actual: ${logger.read().length}, 'Expected: 0`);

logger.debug('log debug');
console.assert(logger.read().length === 0, 'JsLogger: log < level', `Actual: ${logger.read().length}, 'Expected: 0`);

logger.info('log info');
console.assert(logger.read().length === 0, 'JsLogger: log < level', `Actual: ${logger.read().length}, 'Expected: 0`);

logger.warn('log warn');
console.assert(logger.read().length === 0, 'JsLogger: log < level', `Actual: ${logger.read().length}, 'Expected: 0`);

logger.error('log error');
console.assert(logger.read().length === 1, 'JsLogger: log = level', `Actual: ${logger.read().length}, 'Expected: 0`);

logger.fatal('log fatal');
console.assert(logger.read().length === 2, 'JsLogger: log > level', `Actual: ${logger.read().length}, 'Expected: 0`);

console.groupEnd();
