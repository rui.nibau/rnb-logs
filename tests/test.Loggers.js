import JsLogger from '../src/rnb/logs/JsLogger.js';
import Loggers from '../src/rnb/logs/Loggers.js';

console.group('Test: Loggers');
try {
    Loggers.set('test', new JsLogger());
    console.assert(Loggers.has('test') === true, 'Test: Loggers.has');
    console.assert(Loggers.get('test') !== null, 'Test: Loggers.get');
} catch(err) {
    console.error(err);
    console.error('Test: Loggers.set');
}
console.groupEnd();
