import HTMLLogger from '../src/rnb/logs/HTMLLogger.js';

const logger = new HTMLLogger();
logger.level = 'info';
logger.warn('This is a warning message');
logger.info('Testing info message');
logger.error('Testing message with - in it');
logger.info('Testing info message');
logger.error('Testing error message');
logger.info('Testing info message');
logger.info('Testing info message');

globalThis.document.body.append(logger);
